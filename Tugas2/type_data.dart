  // import 'dart:io';
  void main(List <String> Args){
// 1. Soal No. 1 (Membuat kalimat), Terdapat kumpulan variable dengan data string sebagai berikut
/* var word = 'dart';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!'; 
print(word+' '+second+' '+third+' '+fourth+' '+fifth+' '+sixth+' '+seventh); */
// Buatlah agar kata-kata di atas menjadi satu kalimat . Output: Dart is awesome and I love it!

// 2. Soal No.2 Mengurai kalimat (Akses karakter dalam string),
// Terdapat satu kalimat seperti berikut: 
/* String? sentence = "I am going to be Flutter Developer";
String? exampleFirstWord = sentence[0] ;
String? exampleSecondWord = sentence[2] + sentence[3] ;
String? thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]; // lakukan sendiri
String? fourthWord = sentence[11] + sentence[12]; // lakukan sendiri
String? fifthWord = sentence[14] + sentence[15]; // lakukan sendiri
String? sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] + sentence[22] + sentence[23]; // lakukan sendiri
String? seventhWord = sentence[25] + sentence[26] + sentence[27] + sentence[28] + sentence[29] + 
                      sentence[30] + sentence[31] + sentence[32] + sentence[33]; // lakukan sendiri
print('First Word: ' + exampleFirstWord);
print('Second Word: ' + exampleSecondWord);
print('Third Word: ' + thirdWord);
print('Fourth Word: ' + fourthWord);
print('Fifth Word: ' + fifthWord);
print('Sixth Word: ' + sixthWord);
print('Seventh Word: ' + seventhWord); */
// Buat menjadi Output berikut: 
// First word: I
// Second word: am
// Third word: going
// Fourth word: to
// Fifth word: be
// Sixth word: Flutter
// Seventh word: Developer

// 3. Dengan menggunakan I/O pada dart buatlah input dinamis yang akan menginput
// nama depan dan belakang dan jika di enter
// akan menggabungkan nama depan dan belakang
// contoh :
// masukan nama depan :
// hilmy
// masukan nama belakang :
// firdaus
// nama lengkap anda adalah:
// hilmy firdaus
/* print("masukan nama depan");
String? inputFirst = stdin.readLineSync();
print("masukan nama belakang : ");
String? inputLast = stdin.readLineSync();
print("nama lengkap : $inputFirst $inputLast"); */

// 4. Dengan menggunakan operator operasikan variable berikut ini menjadi bentuk
// operasi perkalian, penjumlahan, pengurangan dan pembagian a = 5, b = 10 jadi
// misal a * b = 5 * 10 = 50 dst.
/* int? a = 5;
int? b = 10;
var perkalian = a * b;

ini contoh dari int ke to string bisa menggunakan ini
print("Perkalian :" + perkalian.toString());

print("perkalian : ${a * b}");
print("pembagian : ${a / b}");
print("penambahan : ${a + b}");
print("penguruangan: ${a - b}"); */

}